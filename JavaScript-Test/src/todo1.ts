const addTaskTrigger = document.querySelectorAll('.js-addTask-trigger')[0];
const addTaskTarget = document.querySelectorAll('.js-addTask-target')[0];
const addTaskValue = document.querySelectorAll('.js-addTask-value')[0];

const removeTask = removeButton => {
  const targetTask = removeButton.closest('li');
  addTaskTarget.removeChild(targetTask);
}

const addTask = task => {
  const listItem = document.createElement('li');
  const removeButton = document.createElement('button');
  const completeButton = document.createElement('button');

  removeButton.innerText = '削除';
  removeButton.addEventListener('click', () => removeTask(removeButton));
  listItem.innerText = task;
  listItem.append(removeButton);
  addTaskTarget.appendChild(listItem);

  //追記
  completeButton.innerText = '完了'
  completeButton.addEventListener('click', () => completeTask(completeButton));

  listItem.append(completeButton);
  listItem.append(removeButton);
  addTaskTarget.appendChild(listItem);
};

addTaskTrigger.addEventListener('click', event => {
  const task = addTaskValue.value;
  if (task !== '') {
    addTask(task);
  } else {
    alert('入力しえ')
  }

  addTaskValue.value = '';
})

const completeTask = completeButton => {
  const targetTask = completeButton.closest('li');
  targetTask.classList.add('isComplete');
  targetTask.removeChild(completeButton);
}
