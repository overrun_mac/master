'use strict';
const todo = document.querySelector('#todo');
const priority = document.querySelector('select');
const dateTaxt = document.querySelector('input[type="date"]');
const button = document.querySelector('#submit');
const tables = document.querySelector('table');
const tbody = document.querySelector('tbody');
const storage = localStorage;
let list = [];



document.addEventListener('DOMContentLoaded', () => {
  const json = storage.todoList;
  if (json === undefined) {
    return;
  }
  list = JSON.parse(json);
  for (const item of list) {
    listItemDo(item);
  }
});

const listItemDo = (item) => {
    const tr = document.createElement('tr')
  for (const e in item) {
    const td = document.createElement('td');
    if (e === 'done') {
      const checkbox = document.createElement('input');
      checkbox.type = 'checkbox';
      checkbox.classList.add('end')
      checkbox.checked = item[e];
      td.appendChild(checkbox);
      checkbox.addEventListener('change', checkboxListener);
    } else {
      td.textContent = item[e];
    }
    tr.appendChild(td);
  }
  tbody.append(tr);
  const tes = tr.children;
  for (let i = 0; i < tes.length; i++){
    let flag = tes[i].innerText.indexOf('入力');
    if (flag !== -1) {
      tes[i].classList.add('no-text')
    }
  }
}

const checkboxListener = ev => {
  const currentTr = ev.currentTarget.parentElement.parentElement;
  const trList = Array.from(document.querySelectorAll('tr'));
  const idx = trList.indexOf(currentTr) - 1;
  list[idx].done = ev.currentTarget.checked;
  storage.todoList = JSON.stringify(list)
}

button.addEventListener('click', () => {
  const item = {};
  if (todo.value === "") {
    item.todo = '作業を入力してください';
  } else {
    item.todo = todo.value;
  }
  item.priority = priority.value;
  if (dateTaxt.value === "") {
    item.dateTaxt = '期日を入力してください';
  } else {
    item.dateTaxt = dateTaxt.value;
  }
  item.done = false;


  //インプット要素初期化
  todo.value = "";
  priority.value = "普";
  dateTaxt.value = "";

  listItemDo(item);
  list.push(item);
  storage.todoList = JSON.stringify(list);
});

const clearTable = () => {
  const trList = Array.from(document.querySelectorAll('tr'));
  trList.shift();
  for (const tr of trList) {
    tr.remove();
  }
}

const filterButton = document.createElement('button');
filterButton.textContent = '優先度（高）で絞り込み';
filterButton.id = 'priority';
const main = document.querySelector('main');
main.appendChild(filterButton);

const remove = document.createElement('button');
remove.textContent = '完了したTODOを削除する';
remove.id = 'remove';
const br = document.createElement('br')
main.appendChild(br);
main.appendChild(remove);

filterButton.addEventListener('click', (item) => {
  clearTable();
  for (const item of list) {
    if (item.priority == '高') {
    listItemDo(item);
  }
}
})

remove.addEventListener('click', () => {
  clearTable();
  list = list.filter((item) => item.done == false);
list.forEach((item) => listItemDo(item));
storage.todoList = JSON.stringify(list)
})
